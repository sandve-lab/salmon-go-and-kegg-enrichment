---
title: "Atlantic salmon GO and KEGG enrichment with ClusterProfiler"
output:
  html_document: 
    toc: yes
    toc_float: yes
editor_options: 
  chunk_output_type: console
---

This analysis is an example of how to run GO term and KEGG pathway enrichment of Atlantic salmon genes using the [clusterProfiler](https://bioconductor.org/packages/release/bioc/html/clusterProfiler.html) package (Check out its [documentation](https://yulab-smu.github.io/clusterProfiler-book))

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r loadLibraries, include=FALSE}
library(AnnotationHub)
library(AnnotationDbi)
library(readr)
library(dplyr)
library(clusterProfiler)
```

## Data

The data we will look at includes a list of genes expressed in salmon liver. They are annotated whether they are single-copy or has a duplicate. This will be used to do an overreprepresentation analysis of terms among single-copy genes in comparison to duplicated genes. There is also a measure of normalized variance (resCV) which tells us how much the expression varies among the individuals sampled. This will be used to perform Gene Set Enrichment Analysis (GSEA).

```{r loadData}
tbl <- read_tsv("SsalLiverExpressionVariance.tsv", col_types = "ccdd")

head(tbl)
```


## Gene Ontology enrichment

### Getting the GO gene annotations for Atlantic salmon

GO enrichment in clusterProfiler requires an OrgDb object with GO annotation of the genes. For Atlantic salmon there is an OrgDb available at AnnotationHub

```{r message=FALSE}
# get OrgDb from AnnotationHub
ah <- AnnotationHub()

# list OrgDb's available for Salmo salar
subset(ah, ah$species=="Salmo salar" & ah$rdataclass=="OrgDb")

# retrieve the Ssal OrgDb
SsalOrg <- ah[["AH61820"]]

```

### Overrepresentation analysis of singleton genes

Overrepresentation analysis tells us if the gene-set we are testing are more likely to belong to a certain GO term than a random set of genes. In this case the gene-set is the set of genes that only have one retained copy after the salmonid whole genome duplication. We also have to supply a background gene-set to compare against, in this case we use all the genes in our list which only contains single-copy or duplicates, which means we are contrasting the single-copy genes against the duplicated genes. 


```{r}
single_copy_genes <- filter(tbl, dup_type=="single")$geneID
resGO <- enrichGO(gene = single_copy_genes, 
                  ont = "BP", # can only test one ontology at a time. BP = "Biological process"
                  # universe = tbl$geneID,
                  OrgDb = SsalOrg)

```

#### The result table:

```{r}
DT::datatable(dplyr::select(resGO@result,-geneID),rownames = F)
```

#### Plots

The package contains some commonly used plots for analysis of enrichement results:
```{r}
dotplot(resGO, showCategory=30)
barplot(resGO, showCategory=30)
```

GO enrichment can be hard to analyse because the terms hiererarchical and often highly overlapping, i.e. genes have many GO annotations. Here are some usefull plots:

```{r}
heatplot(resGO, showCategory=30) # analyse overlap of categories
enrichplot::upsetplot(resGO) # analyse overlap of categories
goplot(resGO) # GO terms hierarchy
emapplot(resGO) # Clustering of GO terms based on overlapping genes
```

The following plot is probably more useful if you have the names of the genes and a smaller gene-set:

```{r}
cnetplot(resGO) # this shows the genes connected to the enriched GO terms
```


### Gene Set Enrichment Analysis

```{r}

# ignore the 20% lowest expressed genes
minExpr <- quantile(tbl$liverExp,probs=(0.2))

orderedGenes <- 
  tbl %>%
  filter(liverExp > minExpr) %>%
  arrange(-resCV) %>% 
  with(setNames(resCV,geneID))

gseGOres <- gseGO(geneList = orderedGenes, 
                  ont="BP",
                  pvalueCutoff = 0.2,
                  OrgDb = SsalOrg)

```

#### The result table:

```{r}
DT::datatable(dplyr::select(gseGOres@result,-core_enrichment),rownames = F)
```

#### Plots

```{r}
dotplot(gseGOres, showCategory=30)

heatplot(gseGOres, showCategory=30) # analyse overlap of categories
emapplot(gseGOres) # Clustering of GO terms based on overlapping genes

gseaplot(gseGOres,geneSetID = "GO:0044281",title = "small molecule metabolic process")
gseaplot(gseGOres,geneSetID = "GO:0001558",title = "regulation of cell growth")
gseaplot(gseGOres,geneSetID = "GO:0031929",title = "TOR signaling")
```

## KEGG pathway enrichment

Atlantic salmon is in the KEGG pathway database with the organism ID "sasa".

### Overrepresentation analysis of singleton genes

```{r}
resKEGG <- enrichKEGG(gene = filter(tbl, dup_type=="single")$geneID,
                      universe = tbl$geneID,
                      organism = "sasa")
```

#### The result table:

```{r}
DT::datatable(dplyr::select(resKEGG@result,-geneID),rownames = F)
```

#### Plots

```{r}
dotplot(resKEGG, showCategory=30)
barplot(resKEGG, showCategory=30)

heatplot(resKEGG, showCategory=30) # analyse overlap of categories
enrichplot::upsetplot(resKEGG) # analyse overlap of categories
emapplot(resKEGG) # Clustering of KEGG terms based on overlapping genes

cnetplot(resKEGG) # this shows the genes connected to the enriched KEGG terms
```

### Gene Set Enrichment Analysis

```{r}
gseKEGGres <- gseKEGG(geneList = orderedGenes, 
                      organism = "sasa")

```

#### The result table:

```{r}
DT::datatable(dplyr::select(gseKEGGres@result,-core_enrichment),rownames = F)
```

#### Plots

```{r}
dotplot(gseKEGGres, showCategory=30)

heatplot(gseKEGGres, showCategory=30) # analyse overlap of categories
emapplot(gseKEGGres) # Clustering of GO terms based on overlapping genes

gseaplot(gseKEGGres,geneSetID = "sasa03022",title = "Basal transcription factors")
gseaplot(gseKEGGres,geneSetID = "sasa04010",title="MAPK signaling pathway")
```

